package main

import (
	"springvale/src/interfaces/container"
	"springvale/src/interfaces/server"
)

func main() {
	server.StartService(container.New())
}
