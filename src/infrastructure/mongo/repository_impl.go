package mongo

import (
	"gitlab.com/doniapr-gopkg/pkg/database/mongo"
	"gitlab.com/doniapr-gopkg/pkg/session"
	"go.mongodb.org/mongo-driver/bson"

	"springvale/src/domain/wish"
	"springvale/src/shared/constant"
)

type wishRepository struct {
	mongo mongo.Client
}

func NewWishRepository(db mongo.Client) wish.Repository {
	if db == nil {
		panic("db is nil")
	}
	if err := db.Collection(wish.CollectionName); err != nil {
		panic("wish collection not found")
	}

	return &wishRepository{
		mongo: db,
	}
}

func (w *wishRepository) Save(sess *session.Session, entity wish.Data) (err error) {
	if _, err = w.mongo.InsertOne(entity); err != nil {
		sess.ErrorMessage = err.Error()
		sess.Error(err.Error())
		err = constant.ErrDatabase
		return
	}
	return
}

func (w *wishRepository) FindAll(sess *session.Session) (resp []wish.Data, err error) {
	filter := bson.D{{"isActive", "1"}}
	err = w.mongo.Find(filter, &resp)
	if err != nil {
		sess.ErrorMessage = err.Error()
		sess.Error(err.Error())
		err = constant.ErrDatabase
		return nil, err
	}
	return resp, nil
}
