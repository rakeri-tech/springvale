package wish

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

const CollectionName = "wish"

type Data struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	Name      string             `bson:"name"`
	Presence  string             `bson:"presence"`
	Note      string             `bson:"note"`
	IsActive  string             `bson:"isActive"`
	CreatedAt time.Time          `bson:"createdAt"`
}
