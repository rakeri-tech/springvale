package wish

import "gitlab.com/doniapr-gopkg/pkg/session"

type Repository interface {
	Save(sess *session.Session, entity Data) (err error)
	FindAll(sess *session.Session) (resp []Data, err error)
}
