package server

import (
	"springvale/src/interfaces/container"
	"springvale/src/interfaces/server/http"
)

func StartService(container *container.Container) {
	http.StartHttpService(container)
}
