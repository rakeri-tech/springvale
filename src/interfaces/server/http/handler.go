package http

import "springvale/src/interfaces/container"

type Handler struct {
	wishHandler *wishHandler
}

func SetupHandlers(container *container.Container) *Handler {
	wishHandler := SetupWishHandler(container.WishSvc)

	return &Handler{
		wishHandler: wishHandler,
	}
}
