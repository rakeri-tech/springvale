package http

import (
	"bytes"
	"io/ioutil"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/doniapr-gopkg/pkg/session"
	"gitlab.com/doniapr-gopkg/pkg/utils"
	"gitlab.com/doniapr-gopkg/pkg/vo"
	"gopkg.in/go-playground/validator.v9"

	"springvale/src/interfaces/container"
)

func SetupMiddleware(server *echo.Echo, container *container.Container) {
	cfg := container.Config
	server.Use(func(h echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			// Skip logging on healthcheck endpoint.
			if c.Request().URL.Path == "/" {
				return h(c)
			}

			var bodyBytes []byte
			if c.Request().Body != nil {
				bodyBytes, _ = ioutil.ReadAll(c.Request().Body)
			}

			// Restore the io.ReadCloser to its original state
			c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

			reqId := utils.GenerateThreadId()

			sess := session.New(container.Logger).
				SetThreadID(reqId).
				SetAppName(cfg.App.Name).
				SetAppVersion("0.0").
				SetPort(cfg.App.HttpPort).
				SetSrcIP(c.RealIP()).
				SetURL(c.Request().URL.String()).
				SetMethod(c.Request().Method).
				SetHeader(c.Request().Header).
				SetRequest(string(bodyBytes)).
				SetContext(c.Request().Context())

			sess.T1("Incoming Request")

			c.Set(vo.AppSession, *sess)

			return h(c)
		}
	})

	server.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{echo.GET, echo.POST, echo.OPTIONS},
		AllowHeaders:     []string{"*"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
	}))

	server.HTTPErrorHandler = container.ErrorHandler

	server.Validator = &DataValidator{ValidatorData: validator.New()}

}

type DataValidator struct {
	ValidatorData *validator.Validate
}

func (cv *DataValidator) Validate(i interface{}) error {
	return cv.ValidatorData.Struct(i)
}
