package http

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/doniapr-gopkg/pkg/vo"

	"springvale/src/interfaces/usecase/wish"
)

type wishHandler struct {
	wishSvc wish.Service
}

func SetupWishHandler(svc wish.Service) *wishHandler {
	if svc == nil {
		panic("wish svc is nil")
	}

	return &wishHandler{
		wishSvc: svc,
	}
}

func (h *wishHandler) Save(e echo.Context) error {
	context := vo.Parse(e)
	session := &context.Session

	req := wish.DataRequest{}
	if err := context.BindRequest(&req); err != nil {
		return err
	}

	err := h.wishSvc.Save(session, req)
	if err != nil {
		return err
	}

	return context.Ok(nil)
}

func (h *wishHandler) FindAll(e echo.Context) error {
	context := vo.Parse(e)
	session := &context.Session

	resp, err := h.wishSvc.FindAll(session)
	if err != nil {
		return err
	}

	return context.Ok(resp)
}
