package http

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func SetupRouter(server *echo.Echo, handler *Handler) {
	server.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, "Service up")
	})

	wish := server.Group("/wish")
	{
		wish.POST("/save", handler.wishHandler.Save)
		wish.GET("/find", handler.wishHandler.FindAll)
	}
}
