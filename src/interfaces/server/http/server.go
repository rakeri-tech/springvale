package http

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/labstack/echo/v4"

	"springvale/src/interfaces/container"
)

func StartHttpService(container *container.Container) {
	server := echo.New()

	fmt.Println("Starting the server ...")

	SetupMiddleware(server, container)
	SetupRouter(server, SetupHandlers(container))

	// Start server
	go func() {
		if err := server.Start(fmt.Sprintf(":%d", container.Config.App.HttpPort)); err != nil {
			fmt.Println("Shutting down the server ...")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 10 seconds.
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		container.Logger.Error(err.Error())
	}

	fmt.Println("Shutting down the server success")
}
