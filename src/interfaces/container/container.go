package container

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/doniapr-gopkg/pkg/logger"
	"gitlab.com/doniapr-gopkg/pkg/session"
	"gitlab.com/doniapr-gopkg/pkg/vo"
	"net/http"
	mongo2 "springvale/src/infrastructure/mongo"
	wish2 "springvale/src/interfaces/usecase/wish"
	"springvale/src/shared/constant"
	"springvale/src/shared/database/mongo"

	"springvale/src/shared/config"
)

type Container struct {
	Config  *config.Config
	Logger  logger.Logger
	WishSvc wish2.Service
}

func New() *Container {

	// init cfg
	configPath := fmt.Sprintf("./configs/config.json")
	cfg := config.New(configPath)

	// init logger
	log := logger.New(cfg.Logger)

	// init DB
	mongoDb := mongo.NewMongoDB(&cfg.Database.MongoDB)
	wishRepo := mongo2.NewWishRepository(mongoDb)
	wishSvc := wish2.NewService(wishRepo)

	container := &Container{
		Config:  cfg,
		Logger:  log,
		WishSvc: wishSvc,
	}

	return container
}

func (c *Container) ErrorHandler(err error, context echo.Context) {
	response := constant.DefaultResponseDto{
		ResponseDto: constant.ResponseDto{
			Status:  "99",
			Message: "error",
		},
	}

	sess := context.Get(vo.AppSession).(session.Session)
	sess.SetErrorMessage(err.Error())
	sess.T4(response)
	sess.Error(err.Error())

	err = context.JSON(http.StatusOK, response)
	if err != nil {
		sess.Error(err)
	}
}
