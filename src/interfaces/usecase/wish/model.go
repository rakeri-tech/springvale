package wish

type DataRequest struct {
	Name     string `json:"name"`
	Presence string `json:"presence"`
	Note     string `json:"note"`
	IsActive string `json:"isActive"`
}

type DataResponse struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	Presence  string `json:"presence"`
	Note      string `json:"note"`
	IsActive  string `json:"isActive"`
	CreatedAt string `json:"createdAt"`
}
