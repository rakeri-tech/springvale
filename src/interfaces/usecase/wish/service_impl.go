package wish

import (
	"errors"

	"gitlab.com/doniapr-gopkg/pkg/session"

	"springvale/src/domain/wish"
	"springvale/src/shared/utils"
)

type wishService struct {
	wishRepo wish.Repository
}

func NewService(repo wish.Repository) Service {
	if repo == nil {
		panic("repo is nil")
	}

	return &wishService{
		wishRepo: repo,
	}
}

func (w *wishService) Save(sess *session.Session, req DataRequest) (err error) {
	active := "1"
	if req.IsActive != "" {
		active = req.IsActive
	}
	wishEntity := wish.Data{
		Name:      req.Name,
		Presence:  req.Presence,
		Note:      req.Note,
		IsActive:  active,
		CreatedAt: utils.GetTime(),
	}
	err = w.wishRepo.Save(sess, wishEntity)

	return
}

func (w *wishService) FindAll(sess *session.Session) (resp []DataResponse, err error) {
	entities, err := w.wishRepo.FindAll(sess)
	if err != nil {
		return
	}

	if entities[0].ID.String() == "" {
		err = errors.New("record not found")
		return
	}

	for _, en := range entities {
		dataResp := DataResponse{
			ID:        en.ID.Hex(),
			Name:      en.Name,
			Presence:  en.Presence,
			Note:      en.Note,
			IsActive:  en.IsActive,
			CreatedAt: utils.ConvertTime(en.CreatedAt),
		}
		resp = append(resp, dataResp)
	}

	return
}
