package wish

import "gitlab.com/doniapr-gopkg/pkg/session"

type Service interface {
	Save(sess *session.Session, req DataRequest) (err error)
	FindAll(sess *session.Session) (resp []DataResponse, err error)
}
