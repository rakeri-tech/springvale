package constant

import Error "gitlab.com/doniapr-gopkg/pkg/error"

var (
	ErrDatabase = &Error.ApplicationError{
		ErrorCode: "70",
		Message:   "Database Error",
	}
)

type ResponseDto struct {
	Status  string `valid:"Required" json:"status"`
	Message string `valid:"Required" json:"message"`
}

type DefaultResponseDto struct {
	Data interface{} `json:"data"`
	ResponseDto
}
