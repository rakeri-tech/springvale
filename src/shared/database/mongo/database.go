package mongo

import (
	"context"
	"fmt"
	"strings"
	"time"

	"gitlab.com/doniapr-gopkg/pkg/database/mongo"
)

func NewMongoDB(cfg *DBConfig) mongo.Client {
	fmt.Println("Try New MongoDB...")

	if cfg.Address == nil {
		panic("mongo config address is nil")
	}

	//uri := fmt.Sprintf("mongodb://%s:%s@%s/?authSource=%s", cfg.Username, cfg.Password, strings.Join(cfg.Address, ","), cfg.DBName)
	uri := fmt.Sprintf("mongodb://%s/?authSource=%s", strings.Join(cfg.Address, ","), cfg.DBName)

	if cfg.ReplicaSet != "" {
		uri = uri + "&replicaSet=" + cfg.ReplicaSet
	}

	client, err := mongo.Connect(context.Background(), uri, mongo.ClientOptions{
		MaxPoolSize:     cfg.MaxPoolSize,
		MinPoolSize:     cfg.MinPoolSize,
		MaxConnIdleTime: cfg.MaxConnIdleTime * time.Second,
	})
	if err != nil {
		panic(err)
	}

	return client.DB(cfg.DBName)
}

type DBConfig struct {
	Address         []string      `json:"address" mapstructure:"address"`
	ReplicaSet      string        `json:"replicaSet" mapstructure:"replicaSet"`
	DBName          string        `json:"dbName" mapstructure:"dbName"`
	MaxPoolSize     uint64        `json:"maxPoolSize" mapstructure:"maxPoolSize"`
	MinPoolSize     uint64        `json:"minPoolSize" mapstructure:"minPoolSize"`
	MaxConnIdleTime time.Duration `json:"maxConnIdleTime" mapstructure:"maxConnIdleTime"`
	Username        string        `json:"username" mapstructure:"username"`
	Password        string        `json:"password" mapstructure:"password"`
}
