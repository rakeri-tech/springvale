package utils

import "time"

var (
	JktLoc, _  = time.LoadLocation("Asia/Jakarta")
	TimeLayout = "2006-01-02T15:04:05.000Z"
)

func GetTime() time.Time {
	return time.Now().In(JktLoc)
}

func ConvertTime(t time.Time) string {
	return t.In(JktLoc).Format(TimeLayout)
}
