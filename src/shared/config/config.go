package config

import (
	"fmt"
	"springvale/src/shared/database/mongo"

	"github.com/spf13/viper"
	"gitlab.com/doniapr-gopkg/pkg/logger"
)

type Config struct {
	App      AppConfig      `json:"app"`
	Logger   logger.Options `json:"logger"`
	Database DatabaseConfig `json:"database"`
}

func New(path string) *Config {
	fmt.Println("Try NewConfig ... ")

	viper.SetConfigFile(path)
	viper.SetConfigType("json")

	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}

	defaultConfig := Config{}
	if err := viper.Unmarshal(&defaultConfig); err != nil {
		panic(err)
	}

	return &defaultConfig
}

type AppConfig struct {
	Name     string `json:"name"`
	HttpPort int    `json:"httpPort"`
}

type DatabaseConfig struct {
	MongoDB mongo.DBConfig `json:"mongoDB" mapstructure:"mongoDB"`
}
